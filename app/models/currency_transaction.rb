# Bu model ile bitcoin ve try transferini sağlıyoruz.
class CurrencyTransaction < ApplicationRecord
  # Default ordering
  include Orderable

  # Validations
  validates :user, presence: true

  # Relationships
  belongs_to :user

  validate :one_of_them_must_exists_and_not_zero

  # rubocop:disable AbcSize
  def one_of_them_must_exists_and_not_zero
    if btc
      errors.add(:btc, I18n.t('cant_equal_to_zero')) if btc.zero?
    elsif try
      errors.add(:try, I18n.t('cant_equal_to_zero')) if try.zero?
    else
      errors.add(:id, I18n.t('try_or_btc_must_be_given'))
    end
  end
end
