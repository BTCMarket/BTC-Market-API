# Sistemde duyuruları bu model ile tutuyoruz.
class Notification < ApplicationRecord
  enum status: { not_seen: 10, seen: 20 }
  enum notification_type: { personal: 10, general: 20 }

  # Default ordering
  include Orderable

  # Validations
  validates :title, :content, :user, presence: true

  before_create :set_default_status

  # Relationships
  belongs_to :user

  private

  def set_default_status
    return unless status.nil?
    self.status = Notification.statuses[:not_seen]
  end
end
