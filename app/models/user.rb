# Kullanıcı modeli. Sistemdeki kullanıcıları depolayacağız.
class User < ApplicationRecord
  has_secure_password

  # Validations
  validates :email, presence: true, uniqueness: true
  validates_format_of :email,
                      with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validates :password, presence: true, length: { minimum: 6, maximum: 20 },
                       on: :create

  # Relationships
  has_many :currency_transactions, dependent: :destroy
  has_many :orders
  has_many :notifications, dependent: :destroy

  after_create { set_token! }
  after_create :transfer_1_bitcoin
  after_create :transfer_5000_try
  after_create :send_notification_for_creation

  # User's BitCoin wallet
  def btc_balance
    currency_transactions.sum(:btc)
  end

  # User's Turkish Lira wallet
  def try_balance
    currency_transactions.sum(:try)
  end

  # Transfers 1 bitcoin after creation of user.
  def transfer_1_bitcoin
    currency_transactions.create(btc: 1)
  end

  # Transfers 5000 try after creation of user.
  def transfer_5000_try
    currency_transactions.create(try: 5000)
  end

  # Sends notification after creation.
  def send_notification_for_creation
    notifications.create(title: I18n.t('welcome'),
                         content: I18n.t('welcome_content'))
  end

  # Sets unique & random token for user.
  def set_token!
    payload = {
      email: email,
      rand_key: SecureRandom.hex
    }
    self.token = JWT.encode payload, ApplicationController::SECRET_KEY, 'HS256'

    save!
  end
end
