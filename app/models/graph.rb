# Grafikler için bir class
class Graph
  def self.grouped_sell_orders
    Order.unscoped.active.sell.group(:price)
         .select('SUM(amount) as total_amount, price')
         .order(price: :asc)
  end

  def self.grouped_buy_orders
    Order.unscoped.active.buy.group(:price)
         .select('SUM(amount) as total_amount, price')
         .order(price: :desc)
  end

  def self.months_sell_orders
    Graph.grouped_sell_orders.where('created_at >= ?',
                                    1.month.ago.beginning_of_day).limit(10)
  end

  def self.months_buy_orders
    Graph.grouped_buy_orders.where('created_at >= ?',
                                   1.month.ago.beginning_of_day).limit(10)
  end

  # rubocop:disable AbcSize
  def self.monthly_price_change
    price_list = []

    (0..30).to_a.reverse.each do |day_num|
      order = Order.unscoped.sell.order(price: :asc)
                   .where(created_at: day_num.days.ago.beginning_of_day..
                                      day_num.days.ago.end_of_day).first
      price_list << {
        price: order.price, created_at: order.created_at.strftime('%d-%m-%Y')
      } if order
    end

    price_list
  end
end
