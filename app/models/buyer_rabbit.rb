# Bu class satın almalar içindir.
# rubocop:disable MethodLength
# rubocop:disable AbcSize
class BuyerRabbit
  def initialize(order_id)
    @order = Order.find(order_id)
  end

  def fetch_sell_orders(price)
    Order.active.sell.where('price <= ?', price).order(amount: :asc)
  end

  def send_notifications(user, title, content)
    user.notifications.create title: title, content: content
  end

  def work!
    return false unless @order.active?

    # Fetch sell orders
    sell_orders = fetch_sell_orders @order.price

    sell_orders.each do |sell_order|
      if @order.amount < sell_order.amount
        # Bu emir pasif hale getiriliyor.
        sell_order.amount -= @order.amount

        @order.passive!
        sell_order.save

        # Bakiyeleri transfer ediyoruz.
        @order.user.currency_transactions.create btc: @order.amount
        sell_order.user.currency_transactions.create(
          try: @order.amount * @order.price
        )

        # Bildirim gönderiyoruz.
        send_notifications @order.user, I18n.t('you_bought_btc'),
                           I18n.t('you_bought_btc_message',
                                  price: @order.price, amount: @order.amount)

        send_notifications sell_order.user, I18n.t('you_sold_btc'),
                           I18n.t('you_sold_btc_message',
                                  price: @order.price, amount: @order.amount)

        break
      elsif @order.amount == sell_order.amount
        # İkisi de pasif hale getiriliyor.
        sell_order.passive!

        @order.passive!

        # Bakiyeleri transfer ediyoruz.
        @order.user.currency_transactions.create btc: @order.amount
        sell_order.user.currency_transactions.create(
          try: @order.amount * @order.price
        )

        # Bildirim gönderiyoruz.
        send_notifications @order.user, I18n.t('you_bought_btc'),
                           I18n.t('you_bought_btc_message',
                                  price: @order.price, amount: @order.amount)

        send_notifications sell_order.user, I18n.t('you_sold_btc'),
                           I18n.t('you_sold_btc_message',
                                  price: @order.price, amount: @order.amount)

        break
      elsif @order.amount > sell_order.amount
        # sell order pasif hale getiriliyor.

        sell_order.passive!

        @order.amount -= sell_order.amount
        @order.save

        # Bakiyeleri transfer ediyoruz.
        @order.user.currency_transactions.create btc: sell_order.amount
        sell_order.user.currency_transactions.create(
          try: sell_order.amount * @order.price
        )

        # Bildirim gönderiyoruz.
        send_notifications @order.user, I18n.t('you_bought_btc'),
                           I18n.t('you_bought_btc_message',
                                  price: @order.price,
                                  amount: sell_order.amount)

        send_notifications sell_order.user, I18n.t('you_sold_btc'),
                           I18n.t('you_sold_btc_message',
                                  price: @order.price,
                                  amount: sell_order.amount)
      end
    end
  end
end
