# Bu sayede istediğimiz tüm modellerde yaratılış tarihi en yeni olana göre
# sıralama yapabiliyoruz.
module Orderable
  extend ActiveSupport::Concern

  included do
    default_scope { order('created_at DESC') }
  end
end
