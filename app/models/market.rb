# Market ile ilgili hesaplamalar yapılıyor.
class Market
  # Bu metod ile marketteki açık ve satış emirleri arasından en düşük fiyatı
  # gösteriyoruz.
  def self.lowest_sell
    Order.unscoped.active.sell.minimum(:price)
  end

  # Bu metod ile marketteki açık ve alım emirleri arasından en büyük fiyatı
  # gösteriyoruz.
  def self.highest_buy
    Order.unscoped.active.buy.maximum(:price)
  end

  # Dünkü ortalama fiyat ile, anlık lowest_sell karşılaştırılır,
  # ve fiyat değişimi yüzde ve miktar olarak verilmeli.
  def self.todays_price_change
    {
      amount: Market.todays_avg_price - Market.lowest_sell,
      percentage: (Market.lowest_sell * 100) / Market.todays_avg_price
    }
  end

  # Dünkü ortalama fiyatı hesaplayıp döndürür.
  def self.yesterdays_avg_price
    result = Order.unscoped.sell
                  .where(created_at: 1.day.ago.beginning_of_day..
                                     1.day.ago.end_of_day)
                  .average(:price) || 0.0
    result.round(2)
  end

  # Bugünkü ortalama fiyat
  def self.todays_avg_price
    result = Order.unscoped.sell
                  .where('created_at >= ?', Date.today.beginning_of_day)
                  .average(:price) || 0.0
    result.round(2)
  end
end
