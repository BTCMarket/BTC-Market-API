# Bu model alış ve satış emirlerini depolar.
class Order < ApplicationRecord
  # Default ordering
  include Orderable

  enum status: { passive: 10, active: 20 }
  enum order_status: { sell: 10, buy: 20 }

  # Validations
  validates :amount, numericality: { greater_than: 0 }, presence: true
  validates :price, numericality: { greater_than: 0 }, presence: true
  validates_presence_of :order_status
  validate :check_that_have_enough_balance

  # Relationships
  belongs_to :user

  # hooks
  after_create :take_currency_from_wallet
  after_create :set_notification_for_order

  before_create :set_default_status

  # rubocop:disable AbcSize
  # rubocop:disable CyclomaticComplexity
  # rubocop:disable PerceivedComplexity
  def check_that_have_enough_balance
    if sell?
      if amount && user && user.btc_balance < amount
        errors.add(:amount, I18n.t('you_dont_have_that_btc'))
      end
    elsif buy?
      if amount && price && user && user.try_balance < amount * price
        errors.add(:amount, I18n.t('you_dont_have_that_try'))
      end
    end
  end

  # Set notification for order
  def set_notification_for_order
    user.notifications.create title: I18n.t('order_received'),
                              content: I18n.t('order_received_content')
  end

  # Deposit currency from wallet
  def take_currency_from_wallet
    return false unless active?

    user.currency_transactions.create btc: -amount if sell?
    user.currency_transactions.create try: -amount * price if buy?
  end

  # Calculates amount * price
  def total_try_balance
    amount * price
  end

  def set_default_status
    self.status = Order.statuses[:active] unless status
  end

  # Cancel order.
  def cancel
    return false unless active?

    # transfer currencies
    if sell?
      user.currency_transactions.create(btc: amount)
    elsif buy?
      user.currency_transactions.create(try: amount * price)
    end

    user.notifications.create title: I18n.t('you_cancelled_order'),
                              content: I18n.t('you_cancelled_order_content')

    passive!
  end
end
