# Bu model ile client uygulamasında gösterilecek
# yardım ve belgeleri depoluyoruz.
class Document < ApplicationRecord
  validates :title, :url, presence: true
end
