# Bu class satmalar içindir.
# rubocop:disable MethodLength
# rubocop:disable AbcSize
class SellerRabbit
  def initialize(order_id)
    @order = Order.find(order_id)
  end

  def fetch_buy_orders(price)
    Order.active.buy.where(price: price).order(amount: :asc)
  end

  def send_notifications(user, title, content)
    user.notifications.create title: title, content: content
  end

  def work!
    return false unless @order.active?

    # Fetch sell orders
    buy_orders = fetch_buy_orders @order.price

    buy_orders.each do |buy_order|
      if @order.amount < buy_order.amount
        # Bu emir pasif hale getiriliyor.
        buy_order.amount -= @order.amount

        @order.passive!
        buy_order.save

        # Bakiyeleri transfer ediyoruz.
        @order.user.currency_transactions.create(try: @order.total_try_balance)
        buy_order.user.currency_transactions.create(btc: @order.amount)

        # Bildirim gönderiyoruz.
        send_notifications buy_order.user, I18n.t('you_bought_btc'),
                           I18n.t('you_bought_btc_message',
                                  price: @order.price, amount: @order.amount)

        send_notifications @order.user, I18n.t('you_sold_btc'),
                           I18n.t('you_sold_btc_message',
                                  price: @order.price, amount: @order.amount)

        break
      elsif @order.amount == buy_order.amount
        # İkisi de pasif hale getiriliyor.
        buy_order.passive!
        @order.passive!

        # Bakiyeleri transfer ediyoruz.
        @order.user.currency_transactions.create(try: @order.total_try_balance)
        buy_order.user.currency_transactions.create(btc: @order.amount)

        # Bildirim gönderiyoruz.
        send_notifications buy_order.user, I18n.t('you_bought_btc'),
                           I18n.t('you_bought_btc_message',
                                  price: @order.price, amount: @order.amount)

        send_notifications @order.user, I18n.t('you_sold_btc'),
                           I18n.t('you_sold_btc_message',
                                  price: @order.price, amount: @order.amount)

        break
      elsif @order.amount > buy_order.amount
        # buy_order pasif hale getiriliyor.
        buy_order.passive!

        @order.amount -= buy_order.amount
        @order.save

        # Bakiyeleri transfer ediyoruz.
        @order.user.currency_transactions.create(
          try: buy_order.amount * @order.price
        )
        buy_order.user.currency_transactions.create(btc: buy_order.amount)

        # Bildirim gönderiyoruz.
        send_notifications buy_order.user, I18n.t('you_bought_btc'),
                           I18n.t('you_bought_btc_message',
                                  price: @order.price, amount: buy_order.amount)

        send_notifications @order.user, I18n.t('you_sold_btc'),
                           I18n.t('you_sold_btc_message',
                                  price: @order.price, amount: buy_order.amount)
      end
    end
  end
end
