# Cüzdanı serialize etmek için
class UserWalletSerializer < ActiveModel::Serializer
  attributes :id, :email, :try_balance, :btc_balance
end
