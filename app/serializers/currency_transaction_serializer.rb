# Para transferini serialize etmek için
class CurrencyTransactionSerializer < ActiveModel::Serializer
  attributes :id, :btc, :try, :created_at

  def btc
    object.btc if object.btc
  end

  def try
    object.try if object.try
  end

  def created_at
    I18n.l object.created_at
  end

  belongs_to :user
end
