# Emirleri serialize etmek için
class OrderSerializer < ActiveModel::Serializer
  attributes :id, :amount, :price, :order_status, :created_at, :status

  belongs_to :user

  def created_at
    I18n.l object.created_at
  end
end
