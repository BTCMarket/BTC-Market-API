# Kullanıcıyı serialize etmek için
class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :created_at

  def created_at
    I18n.l object.created_at
  end
end
