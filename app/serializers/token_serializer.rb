# Token Serialize etmek için
class TokenSerializer < ActiveModel::Serializer
  attributes :token

  def token
    object.token
  end
end
