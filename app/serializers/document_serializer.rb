# Belgeyi serialize ediyoruz.
class DocumentSerializer < ActiveModel::Serializer
  attributes :id, :title, :url
end
