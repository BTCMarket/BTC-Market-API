# Profili serialize etmek için
class ProfileSerializer < ActiveModel::Serializer
  attributes :id, :email, :created_at, :total_orders, :sell_orders, :buy_orders

  def total_orders
    object.orders.count
  end

  def sell_orders
    object.orders.sell.count
  end

  def buy_orders
    object.orders.buy.count
  end

  def created_at
    I18n.l object.created_at
  end
end
