# Duyuruyu serialize etmek için
class NotificationSerializer < ActiveModel::Serializer
  attributes :id, :title, :content, :status, :created_at, :notification_type

  belongs_to :user

  def created_at
    I18n.l object.created_at
  end
end
