# NotificationPolicy
class NotificationPolicy
  attr_reader :user, :notification

  def initialize(user, notification)
    @user = user
    @notification = notification
  end

  def index?
    user.present?
  end

  def not_seen?
    user.present?
  end

  def show?
    user.present?
  end

  def set_viewed?
    (user.id == notification.user.id) if user
  end
end
