# OrderPolicy
class OrderPolicy
  attr_reader :user, :order

  def initialize(user, order)
    @user = user
    @order = order
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    true
  end

  def cancel?
    (order.user.id == user.id) if user
  end
end
