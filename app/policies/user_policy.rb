# UserPolicy
class UserPolicy
  attr_reader :user, :user2

  def initialize(user, user2)
    @user = user
    @user2 = user2
  end

  def show?
    true
  end

  def me?
    true
  end

  def wallet?
    true
  end

  def create?
    true
  end

  def update?
    (@user == @user2) if @user && @user2
  end

  def destroy?
    (@user == @user2) if @user && @user2
  end
end
