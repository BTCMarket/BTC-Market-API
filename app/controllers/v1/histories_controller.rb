module V1
  # Transfer geçmişi vs.
  class HistoriesController < ApplicationController
    before_action :authenticate_user!

    def transactions
      @transactions = paginate(current_user.currency_transactions)
      render json: @transactions, meta: pagination_dict(@transactions)
    end

    def orders
      @orders = paginate(current_user.orders)
      render json: @orders, meta: pagination_dict(@orders)
    end
  end
end
