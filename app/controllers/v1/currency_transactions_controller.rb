module V1
  # Currency Transaction larının controllerı burada
  class CurrencyTransactionsController < ApplicationController
    def index
      render json: CurrencyTransaction.limit(15)
    end

    def show
      render json: CurrencyTransaction.find(params[:id])
    end
  end
end
