module V1
  # Market ile bilgiler için controller
  # rubocop:disable MethodLength
  class MarketsController < ApplicationController
    def status
      render json: {
        data: {
          attributes: {
            lowest_sell: Market.lowest_sell,
            highest_buy: Market.highest_buy,
            todays_avg_price: Market.todays_avg_price,
            todays_price_change: {
              amount: Market.todays_price_change[:amount],
              percentage: Market.todays_price_change[:percentage]
            }
          }
        }
      }
    end
  end
end
