module V1
  # Duyuruların larının controllerı burada
  class NotificationsController < ApplicationController
    before_action :authenticate_user!
    before_action :set_notification, except: [:index, :not_seen]

    def index
      authorize :notification

      @notifications = paginate(current_user.notifications)
      render json: @notifications, meta: pagination_dict(@notifications)
    end

    def not_seen
      authorize :notification
      @notifications = current_user.notifications.not_seen

      render json: @notifications
    end

    def show
      render json: @notification
    end

    def set_viewed
      if @notification.seen!
        render json: @notification
      else
        render json: :no_content
      end
    end

    private

    def set_notification
      @notification = current_user.notifications.find(params[:id])
      authorize @notification
    end
  end
end
