module V1
  # Emirler ile ilgili Controller
  # rubocop:disable MethodLength
  class OrdersController < ApplicationController
    before_action :set_order, only: [:show, :update, :destroy, :cancel]
    before_action :set_params, only: [:create, :update]
    before_action :authenticate_user!, except: [:index, :show]

    def index
      authorize :order

      @orders = Order.active.limit(15)
      render json: @orders
    end

    def show
      render json: @order
    end

    def create
      authorize :order

      @order = Order.new(@order_params)
      @order.user = current_user

      if @order.save
        if @order.sell?
          rabbit = SellerRabbit.new(@order.id)
          rabbit.work!
        elsif @order.buy?
          rabbit = BuyerRabbit.new(@order.id)
          rabbit.work!
        end

        render json: @order
      else
        render status: :unprocessable_entity,
               json: @order,
               serializer: ActiveModel::Serializer::ErrorSerializer
      end
    end

    def cancel
      if @order.cancel
        render status: :no_content, json: @order
      else
        render status: :unprocessable_entity,
               json: @order,
               serializer: ActiveModel::Serializer::ErrorSerializer
      end
    end

    private

    def set_order
      @order = Order.find(params[:id])
      authorize @order
    end

    def set_params
      @order_params = params.require(:data).require(:attributes)
                            .permit(:amount, :price, :order_status)
    end
  end
end
