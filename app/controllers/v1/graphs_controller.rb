module V1
  # Grafikler burada
  class GraphsController < ApplicationController
    def monthly
      # sell_orders = Graph.months_sell_orders
      # buy_orders  = Graph.months_buy_orders

      render json: { price_change_data: Graph.monthly_price_change }
    end
  end
end
