module V1
  # kullanıcılar ile ilgili Controller
  class UsersController < ApplicationController
    before_action :set_user,           only: [:show, :update, :destroy]
    before_action :set_params,         only: [:create, :update]
    before_action :authenticate_user!, except: [:show, :create]

    def show
      render json: @user
    end

    def me
      render json: current_user, serializer: ProfileSerializer
    end

    def wallet
      render json: current_user, serializer: UserWalletSerializer
    end

    def create
      @user = User.new(@user_params)

      if @user.save
        render json: @user, status: :created,
               serializer: TokenSerializer
      else
        render status: :unprocessable_entity,
               json: @user,
               serializer: ActiveModel::Serializer::ErrorSerializer
      end
    end

    def update
      @user.assign_attributes(@user_params)

      if @user.save
        render json: @user
      else
        render status: :unprocessable_entity,
               json: @user,
               serializer: ActiveModel::Serializer::ErrorSerializer
      end
    end

    def destroy
      render status: :no_content if @user.destroy
    end

    private

    def set_user
      @user = User.find(params[:id])
      authorize @user
    end

    def set_params
      @user_params = params.require(:data).require(:attributes)
                           .permit(:email, :password, :password_confirmation)
    end
  end
end
