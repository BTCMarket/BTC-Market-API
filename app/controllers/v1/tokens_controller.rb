module V1
  # kullanıcılar ile ilgili Controller
  class TokensController < ApplicationController
    before_action :set_params

    def create
      user = User.find_by(email: @user_params['email'])

      if user && user.authenticate(@user_params['password'])
        user.set_token!

        render json: user, status: :created,
               serializer: TokenSerializer
      else
        user.errors.add(:email, I18n.t('email_and_password_not_matching'))

        render status: :unprocessable_entity,
               json: user, serializer: ActiveModel::Serializer::ErrorSerializer
      end
    end

    private

    def set_params
      @user_params = params.require(:data).require(:attributes)
                           .permit(:email, :password)
    end
  end
end
