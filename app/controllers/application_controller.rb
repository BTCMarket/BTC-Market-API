# App Controller
class ApplicationController < ActionController::API
  include Pundit
  SECRET_KEY = 'o86iq94mfx'.freeze

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def authenticate_user!
    user_not_authorized if current_user.nil?
  end

  def current_user
    User.find_by(token: find_token)
  end

  # JSON API meta tag için gerekli olan sayfalandırma bilgileri.
  def pagination_dict(object)
    {
      current_page: object.current_page,
      next_page: object.next_page,
      prev_page: object.prev_page,
      total_pages: object.total_pages,
      total_count: object.total_count
    }
  end

  # Yollanan veriyi paginate ederek geri gonderen fonksiyon.
  # rubocop:disable AbcSize
  def paginate(data)
    page = 0
    size = 15

    if params.key? 'page'
      page = params[:page][:number] if params[:page].key?('number')
      size = params[:page][:size] if params[:page].key?('size')
    end

    data.page(page).per(size)
  end

  protected

  def find_token
    request.headers['Authorization'].split('token=').last if
              request.headers['Authorization'].present?
  end

  def user_not_authorized
    status = 401
    error = {
      status: status, code: 1, title: 'Unauthorized',
      detail: 'A valid token parameter need for this. '\
              'You have to sign in to make request.'
    }
    render status: status, json: error
  end
end
