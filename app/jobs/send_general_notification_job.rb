# Tüm kullanıcılara gönderilecek, site seviyesindeki bildirimler için
# kullanılacak.
class SendGeneralNotificationJob < ApplicationJob
  queue_as :default

  def perform(title, content)
    User.all.each do |user|
      params = {
        notification_type: Notification.notification_types[:general],
        title: title, content: content
      }

      user.notifications.create(params)
    end
  end
end
