![alt text](https://gitlab.com/BTCMarket/BTC-Market-API/badges/master/build.svg "build")

# BTC-Market API

API Belgeleri [https://btcmarket.gitlab.io/BTC-Market-Docs/](https://btcmarket.gitlab.io/BTC-Market-Docs/) adresinde bulunuyor.

Bu API projesidir. Bu proje backend kısmını barındırır.

Gelen istekler JSON olarak cevaplanır. Format olarak JSONAPI formatında istek
kabul eder ve cevap verir.

JSONAPI için [tıklayınız](http://jsonapi.org/)

Ruby **2.3.1** versiyonu kullanır.

Rails **5.0.0** versiyonu kullanır.
