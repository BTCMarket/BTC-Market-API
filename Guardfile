# A sample Guardfile
# More info at https://github.com/guard/guard#readme

## Uncomment and set this to only include directories you want to watch
# directories %w(app lib config test spec features) \
#  .select{|d| Dir.exists?(d) ? d : UI.warning("Directory #{d} does not exist")}

## Note: if you are using the `directories` clause above and you are not
## watching the project directory ('.'), then you will want to move
## the Guardfile to a watched dir and symlink it back, e.g.
#
#  $ mkdir config
#  $ mv Guardfile config/
#  $ ln -s config/Guardfile .
#
# and, you'll have to watch "config/Guardfile" instead of "Guardfile"

guard :minitest, spring: 'bin/rails test' do
  # Fixtures'in ilgili modelini degisiklik durumunda test etmek icin,
  # Cogul olan fixtures adini tekil yapmamiz lazim.
  # Bunun icin rails'in inflections kutuphanesini ekliyoruz.
  # Boylece 'singularize' methodunu kullanabiliyoruz.
  # Ayni sekilde modelin, controllerini bulmak icin pluralize kullaniyoruz.
  require 'rails'
  require 'active_support/inflections'

  # with Minitest::Unit
  watch(%r{^test/(.*)\/?(.*)_test\.rb$})
  watch(%r{^test/test_helper\.rb$})      { 'test' }

  # Rails 4
  watch(%r{^app/(.+)\.rb$})                               { |m| "test/#{m[1]}_test.rb" }
  watch(%r{^app/controllers/application_controller\.rb$}) { 'test/controllers' }
  watch(%r{^app/controllers/(.+)_controller\.rb$})        { |m| "test/integration/#{m[1]}_test.rb" }
  watch(%r{^app/views/(.+)_mailer/.+})                    { |m| "test/mailers/#{m[1]}_mailer_test.rb" }
  # watch(%r{^test/.+_test\.rb$})
  # watch(%r{^test/test_helper\.rb$}) { 'test' }

  # Fixtures guncellenince ilgili model testi de calissin
  watch(%r{^test/fixtures/(.+)\.yml$})                    { |m| "test/models/#{m[1].singularize}_test.rb" }

  # Model guncellenince ilgili controller da guncellensin.
  watch(%r{^app/models/(.+)\.rb$})                        { |m| "test/controllers/v1/#{m[1].pluralize}_controller_test.rb" }
end

guard :rubocop do
  watch(%r{.+\.rb$})
  watch(%r{(?:.+/)?\.rubocop\.yml$}) { |m| File.dirname(m[0]) }
end
