require 'test_helper'

module V1
  # Orders Controller testleri
  class NotificationsControllerTest < ActionDispatch::IntegrationTest
    setup do
      @notification = notifications(:non_viewed)
      @user = users(:joe)
      @headers = { 'Authorization' => "Token token=#{@user.reload.token}" }
    end

    test 'giriş yaparak hepsini listeleyebilmeli' do
      get v1_notifications_url, headers: @headers

      resp = JSON.parse(response.body)

      assert_response :success
      assert_equal @user.notifications.count,
                   resp['data'].count

      # Sayfalama testleri
      assert resp['meta'].key?('current_page')
      assert_equal resp['meta']['current_page'], 1
      assert_equal resp['meta']['total_count'],
                   @user.notifications.count
    end

    test 'giriş yaparak sadece okunmayanları listeleyebilmeli' do
      get not_seen_v1_notifications_url, headers: @headers

      assert_response :success
      assert_equal JSON.parse(response.body)['data'].count,
                   @user.notifications.not_seen.count
    end

    test 'giriş yaparak birinin detaylarına bakabilmeli' do
      get v1_notification_url(@notification), headers: @headers

      assert_response :success
    end

    test 'giriş yaparak okundu olarak işaretleyebilmeli' do
      put set_viewed_v1_notification_url(@notification), headers: @headers

      assert_response :success
      assert_equal JSON.parse(response.body)['data']['attributes']['status'],
                   'seen'
    end

    test 'giriş yapmadan hiçbir eylem yapamamalı' do
      get v1_notifications_url
      assert_response :unauthorized

      get v1_notification_url(@notification)
      assert_response :unauthorized

      put set_viewed_v1_notification_url(@notification)
      assert_response :unauthorized
    end
  end
end
