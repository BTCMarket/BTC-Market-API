require 'test_helper'

module V1
  # Orders Controller testleri
  class OrdersControllerTest < ActionDispatch::IntegrationTest
    setup do
      @order = orders(:buy)
      @user = @order.user

      @headers = { 'Authorization' => "Token token=#{@user.reload.token}" }

      @order_params = {
        data: {
          attributes: {
            order_status: 'sell',
            price: 100,
            amount: 2
          }
        }
      }
    end

    test 'giriş yapmayan kullanıcı istenilen şekilde işlemleri yapabilmeli' do
      # Giriş yapmadan listeleyebilmeli
      get v1_orders_url
      assert_response :success

      # Giriş yapmadan bir tanesini görüntüleyebilmeli
      get v1_order_url(@order)
      assert_response :success

      # Giriş yapmadan yeni ekleyememeli
      assert_no_difference 'Order.count' do
        post v1_orders_url, params: @order_params

        assert_response :unauthorized
      end

      # Giriş yapmadan emri iptal edememeli
      put cancel_v1_order_url(@order)

      assert_response :unauthorized
      assert @order.reload.active?
    end

    test 'hepsini listeleyebilmeli' do
      get v1_orders_url

      assert_response :success
    end

    test 'birinin detaylarına bakabilmeli' do
      get v1_order_url(@order)

      assert_response :success
    end

    test 'giriş yapan kullanıcı yeni oluşturabilmeli' do
      assert_difference 'Order.count' do
        post v1_orders_url, params: @order_params, headers: @headers

        assert_response :success
      end
    end

    test 'giriş yapan kullanıcı emrini iptal edebilmeli' do
      put cancel_v1_order_url(@order), headers: @headers

      assert_response :success
      refute @order.reload.active?
    end
  end
end
