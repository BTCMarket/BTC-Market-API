require 'test_helper'

module V1
  # CurrencyTransaction Controller testleri
  class CurrencyTransactionsControllerTest < ActionDispatch::IntegrationTest
    setup do
      @transaction = currency_transactions(:one)
    end

    test 'hepsini listeleyebilmeli' do
      get v1_currency_transactions_url
      assert_response :success
    end

    test 'birinin detaylarına bakabilmeli' do
      get v1_currency_transaction_url(@transaction)
      assert_response :success
    end
  end
end
