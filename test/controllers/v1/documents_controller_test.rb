require 'test_helper'

module V1
  # Orders Controller testleri
  class DocumentsControllerTest < ActionDispatch::IntegrationTest
    setup do
      @document = documents(:yardim)
    end

    test 'hepsini listeleyebilmeli' do
      get v1_documents_url

      assert_response :success
    end

    test 'birinin detaylarına bakabilmeli' do
      get v1_document_url(@document)

      assert_response :success
    end
  end
end
