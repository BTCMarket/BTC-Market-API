require 'test_helper'

module V1
  # History Controller Testleri
  class HistoriesControllerTest < ActionDispatch::IntegrationTest
    setup do
      User.all.each(&:set_token!)
      @user = users(:donald)

      @headers = { 'Authorization' => "Token token=#{@user.reload.token}" }
    end

    test 'giriş yapmış kullanıcı için geçmiş transferler görüntülenmeli' do
      get v1_histories_transactions_url, headers: @headers
      assert_response :success
    end

    test 'giriş yapmış kullanıcı sayfalama yapabilmeli' do
      # Sadece 1 bildirimi var. 2 tane daha ekleyelim.
      @user.currency_transactions.create btc: 0.7
      @user.currency_transactions.create btc: 3.7

      # Parametreler
      params = { page: { number: 1, size: 1 } }

      get v1_histories_transactions_url, params: params, headers: @headers
      assert_response :success

      resp = JSON.parse(response.body)

      # Sayfalama testleri
      assert resp['meta'].key?('current_page')
      assert_equal resp['meta']['current_page'], 1
      assert_equal resp['meta']['total_count'],
                   @user.currency_transactions.count

      # Sayfalama linkleri testleri
      assert resp['links'].key?('self')
      assert resp['links'].key?('next')
    end

    test 'giriş yapmış kullanıcı için geçmiş emirler görüntülenmeli' do
      params = { page: { number: 1, size: 1 } }
      get v1_histories_orders_url, params: params, headers: @headers
      assert_response :success

      resp = JSON.parse(response.body)

      # Sayfalama testleri
      assert resp['meta'].key?('current_page')
      assert_equal resp['meta']['current_page'], 1
      assert_equal resp['meta']['total_count'],
                   @user.orders.count

      # Sayfalama linkleri testleri
      assert resp['links'].key?('self')
      assert resp['links'].key?('next')
    end

    test 'giriş yapmamış kullanıcı geçmiş transferleri görememeli' do
      get v1_histories_transactions_url
      assert_response :unauthorized
    end

    test 'giriş yapmamış kullanıcı geçmiş emirleri görememeli' do
      get v1_histories_orders_url
      assert_response :unauthorized
    end
  end
end
