require 'test_helper'

module V1
  # Users Controller testleri
  class UsersControllerTest < ActionDispatch::IntegrationTest
    setup do
      @user = users(:joe)
      @user_params = {
        data: {
          attributes: {
            email: 'yigit@example.com',
            password: '123456',
            password_confirmation: '123456'
          }
        }
      }

      User.all.each(&:set_token!)

      @headers = { 'Authorization' => "Token token=#{@user.reload.token}" }
    end

    test 'birinin detaylarına bakabilmeli' do
      get v1_user_url(@user)

      assert_response :success
    end

    test 'yeni oluşturabilmeli' do
      assert_difference 'User.count' do
        post v1_users_url, params: @user_params

        assert_response :created
        assert JSON.parse(response.body)['data']['attributes']['token'],
               User.last.token
      end
    end

    test 'giriş yapan kullanıcı güncelleyebilmeli' do
      put v1_user_url(@user), params: @user_params, headers: @headers

      assert_response :success
    end

    test 'giriş yapmayan kullanıcı güncelleyememeli' do
      put v1_user_url(@user), params: @user_params

      assert_response :unauthorized
    end

    test 'giriş yapan kullanıcı silebilmeli' do
      assert_difference 'User.count', -1 do
        delete v1_user_url(@user), headers: @headers

        assert_response :success
      end
    end

    test 'giriş yapmayan kullanıcı silememeli' do
      assert_no_difference 'User.count' do
        delete v1_user_url(@user)

        assert_response :unauthorized
      end
    end

    test 'kullanıcı me adresine girerek profilini görebilmeli' do
      get me_v1_users_url, headers: @headers

      assert_response :success
      resp = JSON.parse(response.body)['data']['attributes']

      assert resp.key? 'total_orders'
      assert resp.key? 'sell_orders'
      assert resp.key? 'buy_orders'

      assert_equal resp['total_orders'], @user.orders.count
      assert_equal resp['sell_orders'], @user.orders.sell.count
      assert_equal resp['buy_orders'], @user.orders.buy.count
    end

    test 'kullanıcı cüzdanını görüntüleyebilmeli' do
      get wallet_v1_users_url, headers: @headers

      assert_response :success
    end

    test 'kullanıcı giriş yapmamışsa me adresine girememeli' do
      get me_v1_users_url

      assert_response :unauthorized
    end
  end
end
