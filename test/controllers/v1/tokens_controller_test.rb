require 'test_helper'

module V1
  # Orders Controller testleri
  class TokensControllerTest < ActionDispatch::IntegrationTest
    test 'yeni token oluşturabilmeli' do
      token_params = {
        data: {
          attributes: {
            email: users(:joe).email,
            password: '123456'
          }
        }
      }

      post v1_tokens_url, params: token_params
      assert_response :created
      assert_equal JSON.parse(response.body)['data']['attributes']['token'],
                   users(:joe).reload.token
    end
  end
end
