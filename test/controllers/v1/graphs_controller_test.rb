require 'test_helper'

module V1
  # Grafikler Controller testleri
  class GraphsControllerTest < ActionDispatch::IntegrationTest
    test 'aylık değişim grafiklerini sunabilmeli' do
      get v1_graphs_monthly_url
      assert_response :success
    end
  end
end
