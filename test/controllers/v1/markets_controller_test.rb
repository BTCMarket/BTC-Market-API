require 'test_helper'

class MarketsControllerTest < ActionDispatch::IntegrationTest
  test 'market ile bilgiler gösterilmeli' do
    get v1_markets_status_url

    resp = JSON.parse(response.body)

    assert_response :success

    assert resp['data']['attributes'].key? 'lowest_sell'
    assert resp['data']['attributes'].key? 'highest_buy'
    assert resp['data']['attributes'].key? 'todays_avg_price'
    assert resp['data']['attributes'].key? 'todays_price_change'

    assert_equal Market.highest_buy.to_s,
                 resp['data']['attributes']['highest_buy']
    assert_equal Market.lowest_sell.to_s,
                 resp['data']['attributes']['lowest_sell']
    assert_equal Market.todays_avg_price.to_s,
                 resp['data']['attributes']['todays_avg_price']
  end
end
