require 'test_helper'

class OrderPolicyTest < PolicyAssertions::Test
  setup { @order = orders(:sell) }

  test 'index herkes için kullanılabilir olmalı' do
    assert_permit User, Order, 'index?'
  end

  test 'show herkes için kullanılabilir olmalı' do
    assert_permit User, Order, 'show?'
  end

  test 'create giriş yapmış kullanıcılar için kullanılabilir olmalı' do
    assert_permit @order.user, Order, 'create?'
  end

  test 'cancel kullanıcının kendi emri için kullanılabilir olmalı' do
    assert_permit @order.user, @order, 'cancel?'
  end

  test 'başka kullanıcının emri için cancel verilememeli' do
    refute_permit users(:donald), @order, 'cancel?'
  end
end
