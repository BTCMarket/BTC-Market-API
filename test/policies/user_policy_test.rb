require 'test_helper'

class UserPolicyTest < PolicyAssertions::Test
  setup { @user = users(:joe) }

  test 'show herkes için kullanılabilir olmalı' do
    assert_permit User, User, 'show?'
  end

  test 'me her kullanıcı için olmalı' do
    assert_permit User, User, 'me?'
  end

  test 'wallet her kullanıcı için olmalı' do
    assert_permit User, User, 'wallet?'
  end

  test 'create herkes için kullanılabilir olmalı' do
    assert_permit User, User, 'create?'
  end

  test 'update sadece kendi için kullanılabilir olmalı' do
    assert_permit @user, @user, 'update?'
    refute_permit @user, users(:donald), 'update?'
  end

  test 'destroy sadece kendi için kullanılabilir olmalı' do
    assert_permit @user, @user, 'destroy?'
    refute_permit @user, users(:donald), 'destroy?'
  end
end
