require 'test_helper'

class NotificationPolicyTest < PolicyAssertions::Test
  setup do
    @notification = notifications(:one)
    @user = users(:joe)
  end

  test 'index giriş yapmış kullanıcılar için kullanılabilir olmalı' do
    assert_permit @user, Notification, 'index?'
  end

  test 'not seen giriş yapmış kullanıcılar için kullanılabilir olmalı' do
    assert_permit @user, Notification, 'not_seen?'
  end

  test 'show giriş yapmış kullanıcılar için kullanılabilir olmalı' do
    assert_permit @user, @notification, 'show?'
  end

  test 'set_viewed giriş yapmış kullanıcılar için kullanılabilir olmalı' do
    assert_permit @user, @notification, 'set_viewed?'
  end

  test 'set_viewed başka bir kullanıcı tarafından kullanılamamalı' do
    refute_permit @user, users(:donald).notifications.first, 'set_viewed?'
  end
end
