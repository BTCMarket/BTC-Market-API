require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  setup do
    @new_order = Order.new(order_status: Order.order_statuses[:sell],
                           price: 300, amount: 0.5, user: users(:donald))
  end

  test 'yeter şartlar sağlandığında geçerli olmalı' do
    assert @new_order.valid?
  end

  test 'user alanı boş olmamalı' do
    @new_order.user = nil

    refute @new_order.valid?
  end

  test 'order_status boş olduğunda geçersiz olmalı' do
    @new_order.order_status = nil

    refute @new_order.valid?
  end

  test 'order_status sell veya buy olabilir' do
    @new_order.order_status = Order.order_statuses[:sell]
    assert @new_order.valid?

    @new_order.order_status = Order.order_statuses[:buy]
    assert @new_order.valid?
  end

  test 'amount boş olduğunda geçersiz olmalı' do
    @new_order.amount = nil

    refute @new_order.valid?
  end

  test 'amount sıfır olduğunda geçersiz olmalı' do
    @new_order.amount = 0

    refute @new_order.valid?
  end

  test 'amount negatif olduğunda geçersiz olmalı' do
    @new_order.amount = -1

    refute @new_order.valid?
  end

  test 'price boş olduğunda geçersiz olmalı' do
    @new_order.price = nil

    refute @new_order.valid?
  end

  test 'price sıfır olduğunda geçersiz olmalı' do
    @new_order.price = 0

    refute @new_order.valid?
  end

  test 'price negatif olduğunda geçersiz olmalı' do
    @new_order.price = -1

    refute @new_order.valid?
  end

  test 'yeni kaydedilebilmeli' do
    assert @new_order.save
  end

  test 'yeni kaydedildikten sonra bildirim eklenmeli' do
    assert_difference('@new_order.user.notifications.count') do
      assert @new_order.save

      assert @new_order.user.notifications.count > 0
    end
  end

  test 'cancel komutu ile emir iptal edilebilmeli' do
    order = orders(:sell)

    assert order.cancel
    assert order.reload.passive?
  end

  test 'sipariş iptal edildikten sonra bildirim gönderilmeli' do
    order = orders(:sell)

    assert_difference('order.user.notifications.count') do
      assert order.cancel
    end
  end

  test 'satış emri iptal edildiğinde btc hesaba aktarılmalı' do
    @new_order.sell!

    assert_difference('@new_order.user.btc_balance', @new_order.amount) do
      @new_order.cancel
    end
  end

  test 'alış emri iptal edildiğinde try hesaba aktarılmalı' do
    @new_order.buy!

    expected_diff = @new_order.reload.amount * @new_order.reload.price

    assert_difference('@new_order.user.try_balance', expected_diff) do
      @new_order.cancel
    end
  end

  test 'satış emri verildiğinde btc bakiyesi hesaptan düşmeli' do
    @new_order.order_status = Order.order_statuses[:sell]

    assert_difference('@new_order.user.btc_balance', -@new_order.amount) do
      @new_order.save
    end
  end

  test 'alım emri verildiğinde try bakiyesi hesaptan düşmeli' do
    @new_order.order_status = Order.order_statuses[:buy]
    expected_diff = -@new_order.amount * @new_order.price

    assert_difference('@new_order.user.try_balance', expected_diff) do
      @new_order.save
    end
  end

  test 'satış emri verilirken hesapta bakiye kontrolü olmalı' do
    @new_order.amount = 10_000
    refute @new_order.valid?
  end

  test 'alış emri verilirken hesapta bakiye kontrolü olmalı' do
    @new_order.amount = 100_000
    @new_order.price  = 100_000

    # bu işlem için hesapta 10000000000 try olması gerekiyor.
    # ki bu kadar try yok.

    refute @new_order.valid?
  end
end
