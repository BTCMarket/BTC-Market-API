require 'test_helper'

class BuyerRabbitSmallerTest < ActiveSupport::TestCase
  # Alım emri ile satım emri karşılaştırıldığında, alım emrinin miktarı
  # küçük ise testleri.

  setup do
    @order = Order.create(order_status: Order.order_statuses[:buy],
                          amount: 0.3, price: 100, user: users(:donald))
    @rabbit = BuyerRabbit.new(@order.id)
  end

  test 'work! ile bakiye transfer edilmeli' do
    assert_difference('@order.user.btc_balance', @order.amount) do
      @rabbit.work!
    end
  end

  test 'work! ile alım emri pasif hale getirilmeli' do
    @rabbit.work!

    refute @order.reload.active?
  end

  test 'work! ile alıcıya bildirim gönderilmeli' do
    assert_difference('@order.user.notifications.count') do
      @rabbit.work!
    end
  end

  test 'work! ile satıcıya bildirim gönderilmeli' do
    assert_difference('Notification.count', 2) do
      @rabbit.work!
    end
  end
end

class BuyerRabbitEqualTest < ActiveSupport::TestCase
  # Alım emri ile satım emri karşılaştırıldığında, alım emrinin miktarı
  # eşit ise testleri.

  setup do
    @order = Order.create(order_status: Order.order_statuses[:buy],
                          amount: 0.5, price: 100, user: users(:donald))
    @rabbit = BuyerRabbit.new(@order.id)
  end

  test 'work! ile alım emri pasif hale gelmeli' do
    @rabbit.work!

    refute @order.reload.active?
  end

  test 'work! ile satım emri de pasif hale gelmeli' do
    assert_difference('Order.passive.count', 2) do
      @rabbit.work!
    end
  end

  test 'work! ile bakiyeler transfer edilmeli' do
    assert_difference('@order.user.btc_balance', @order.amount) do
      @rabbit.work!
    end
  end

  test 'work! ile alıcıya bildirim gönderilmeli' do
    assert_difference('@order.user.notifications.count') do
      @rabbit.work!
    end
  end

  test 'work! ile satıcıya bildirim gönderilmeli' do
    assert_difference('Notification.count', 2) do
      @rabbit.work!
    end
  end
end

class BuyerRabbitBiggerTest < ActiveSupport::TestCase
  # Almak istenen miktar ile ilk emirdeki ilan karşılaştırıldığında,
  # ilk emirdeki miktarın küçük olma durumu.

  setup do
    @order = Order.create(order_status: Order.order_statuses[:buy],
                          amount: 1, price: 100, user: users(:donald))
    @rabbit = BuyerRabbit.new(@order.id)
  end

  test 'work! ile 2 ilan pasif hale gelmeli' do
    assert_difference('Order.passive.count', 2) do
      @rabbit.work!
    end
  end

  test 'work! ile 4 bildirim gitmeli' do
    assert_difference('Notification.count', 4) do
      @rabbit.work!
    end
  end

  test 'work! ile son ilanın miktarı 0.1 olmalı' do
    @rabbit.work!

    latest_order = Order.active.sell.find_by(price: @order.price)

    assert_equal 0.1, latest_order.amount
  end

  test 'work! ile 4 transfer yapılmalı' do
    assert_difference('CurrencyTransaction.count', 4) do
      @rabbit.work!
    end
  end
end

class BuyerRabbitNotFoundTest < ActiveSupport::TestCase
  # Alım ilanını karşılayan bir ilan bulunamadığında.

  setup do
    @order = Order.create(order_status: Order.order_statuses[:buy],
                          amount: 0.5, price: 50, user: users(:donald))
    @rabbit = BuyerRabbit.new(@order.id)
  end

  test 'work! komutu verildiğinde hiç transfer yapılmamalı' do
    assert_no_difference('CurrencyTransaction.count') do
      @rabbit.work!
    end
  end

  test 'work! komutu verildiğinde hiç bildirim gitmemeli' do
    assert_no_difference('Notification.count') do
      @rabbit.work!
    end
  end
end
