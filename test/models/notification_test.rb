require 'test_helper'

class NotificationTest < ActiveSupport::TestCase
  setup do
    @new_notification = Notification.new(title: 'lorem', content: 'ipsum',
                                         user: users(:joe))
  end

  test 'seen! metodu ile duyuru okundu olmalı' do
    notification = notifications(:one)
    refute notification.seen?

    notification.seen!
    assert notification.reload.seen?
  end

  test 'yeter şartlarda geçerli olmalı' do
    assert @new_notification.valid?
  end

  test 'yeni oluşturulduğunda personal bir duyuru olmalı' do
    assert @new_notification.save
    assert @new_notification.reload.personal?
  end

  test 'user alanı boş olmamalı' do
    @new_notification.user = nil

    refute @new_notification.valid?
  end

  test 'title alanı boş olmamalı' do
    @new_notification.title.clear

    refute @new_notification.valid?
  end

  test 'content alanı boş olmamalı' do
    @new_notification.content.clear

    refute @new_notification.valid?
  end

  test 'oluşturulduğunda viewed false olmalı' do
    @new_notification.save

    refute @new_notification.reload.seen?
  end

  test 'yeni oluşturabilmeli' do
    assert_difference('Notification.count') do
      @new_notification.save
    end
  end
end
