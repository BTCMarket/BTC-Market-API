require 'test_helper'

class CurrencyTransactionTest < ActiveSupport::TestCase
  setup do
    @transaction = currency_transactions(:one)
    @new_transaction = CurrencyTransaction.new(user: users(:joe))
  end

  test 'yeter şartlar sağlandığında geçerli olmalı' do
    @new_transaction.btc = 10

    assert @new_transaction.valid?
  end

  test 'user_id boş olmamalı' do
    @new_transaction.user = nil

    refute @new_transaction.valid?
  end

  test 'btc negatif olabilmeli' do
    @new_transaction.btc = -1

    assert @new_transaction.valid?
  end

  test 'btc pozitif olabilmeli' do
    @new_transaction.btc = 1

    assert @new_transaction.valid?
  end

  test 'btc sıfır olmamalı' do
    @new_transaction.btc = 0

    refute @new_transaction.valid?
  end

  test 'try negatif olabilmeli' do
    @new_transaction.try = -1

    assert @new_transaction.valid?
  end

  test 'try pozitif olabilmeli' do
    @new_transaction.try = 1

    assert @new_transaction.valid?
  end

  test 'try sıfır olmamalı' do
    @new_transaction.try = 0

    refute @new_transaction.valid?
  end

  test 'yeni oluşturabilmeli' do
    @new_transaction.try = 10

    assert_difference 'CurrencyTransaction.count' do
      assert @new_transaction.save
    end
  end

  test 'eğer btc veya try yoksa geçerli olmamalı' do
    refute @new_transaction.valid?
  end
end
