require 'test_helper'

class SellerRabbitSmallerTest < ActiveSupport::TestCase
  # Alım emri ile satım emri karşılaştırıldığında, alım emrinin miktarı
  # küçük ise testleri.

  setup do
    @order = Order.create(order_status: Order.order_statuses[:sell],
                          amount: 0.3, price: 90, user: users(:joe))
    @rabbit = SellerRabbit.new(@order.id)
  end

  test 'work! ile satış emri pasif hale getirilmeli' do
    @rabbit.work!

    refute @order.reload.active?
  end

  test 'work! ile satıştan sonra bakiye transfer edilmeli' do
    assert_difference('@order.user.try_balance', @order.total_try_balance) do
      @rabbit.work!
    end
  end

  test 'work! ile alıcıya da bakiye transfer edilmeli' do
    assert_difference('CurrencyTransaction.count', 2) do
      @rabbit.work!
    end
  end

  test 'work! ile satıcıya bildirim gönderilmeli' do
    assert_difference('@order.user.notifications.count') do
      @rabbit.work!
    end
  end

  test 'work! ile alıcıya da bildirim gönderilmeli' do
    assert_difference('Notification.count', 2) do
      @rabbit.work!
    end
  end
end

class SellerRabbitEqualTest < ActiveSupport::TestCase
  # Alım emri ile satım emri karşılaştırıldığında, alım emrinin miktarı
  # eşit ise testleri.

  setup do
    @order = Order.create(order_status: Order.order_statuses[:sell],
                          amount: 0.5, price: 90, user: users(:joe))
    @rabbit = SellerRabbit.new(@order.id)
  end

  test 'work! ile satış emri pasif hale getirilmeli' do
    @rabbit.work!

    refute @order.reload.active?
  end

  test 'work! ile alış emri de pasif hale getirilmeli' do
    assert_difference('Order.passive.count', 2) do
      @rabbit.work!
    end
  end

  test 'work! ile satıştan sonra bakiye transfer edilmeli' do
    assert_difference('@order.user.try_balance', @order.total_try_balance) do
      @rabbit.work!
    end
  end

  test 'work! ile alıcıya da bakiye transfer edilmeli' do
    assert_difference('CurrencyTransaction.count', 2) do
      @rabbit.work!
    end
  end

  test 'work! ile satıcıya bildirim gönderilmeli' do
    assert_difference('@order.user.notifications.count') do
      @rabbit.work!
    end
  end

  test 'work! ile alıcıya da bildirim gönderilmeli' do
    assert_difference('Notification.count', 2) do
      @rabbit.work!
    end
  end
end

class SellerRabbitBiggerTest < ActiveSupport::TestCase
  # Almak istenen miktar ile ilk emirdeki ilan karşılaştırıldığında,
  # ilk emirdeki miktarın küçük olma durumu.

  setup do
    @order = Order.create(order_status: Order.order_statuses[:sell],
                          amount: 1, price: 90, user: users(:joe))
    @rabbit = SellerRabbit.new(@order.id)
  end

  test 'work! ile 2 ilan pasif hale gelmeli' do
    assert_difference('Order.passive.count', 2) do
      @rabbit.work!
    end
  end

  test 'work! ile 4 bildirim gitmeli' do
    assert_difference('Notification.count', 4) do
      @rabbit.work!
    end
  end

  test 'work! ile son ilanın miktarı 0.1 olmalı' do
    @rabbit.work!

    latest_order = Order.active.buy.find_by(price: @order.price)

    assert_equal 0.1, latest_order.amount
  end

  test 'work! ile 4 transfer yapılmalı' do
    assert_difference('CurrencyTransaction.count', 4) do
      @rabbit.work!
    end
  end
end

class SellerRabbitNotFoundTest < ActiveSupport::TestCase
  # Satım ilanını karşılayan bir ilan bulunamadığında.

  setup do
    @order = Order.create(order_status: Order.order_statuses[:sell],
                          amount: 0.5, price: 70, user: users(:joe))
    @rabbit = SellerRabbit.new(@order.id)
  end

  test 'work! komutu verildiğinde hiç transfer yapılmamalı' do
    assert_no_difference('CurrencyTransaction.count') do
      @rabbit.work!
    end
  end

  test 'work! komutu verildiğinde hiç bildirim gitmemeli' do
    assert_no_difference('Notification.count') do
      @rabbit.work!
    end
  end
end
