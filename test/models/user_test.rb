require 'test_helper'

class UserTest < ActiveSupport::TestCase
  setup do
    @new_user = User.new(email: 'sample@example.com',
                         password: '123456',
                         password_confirmation: '123456')
  end

  test 'yeter şartlar sağlandığında geçerli olmalı' do
    assert @new_user.valid?
  end

  test 'set_token istenildiği gibi çalışmalı' do
    user = users(:joe)
    user.set_token!

    first = user.token

    assert_not_nil user.token

    user.set_token!
    user.reload

    assert_not_equal user.token, first
    assert_not_nil   user.token
  end

  test 'kullanıcı oluşturulduğunda hesabına 1 bitcoin transfer edilmeli' do
    @new_user.save

    assert_equal @new_user.btc_balance, 1
  end

  test 'kullanıcı oluşturulduktan sonra hesabına bildirim gitmeli' do
    assert_difference('Notification.count') do
      @new_user.save
      assert_equal @new_user.notifications.count, 1
    end
  end

  test 'kullanıcı oluşturulduğunda hesabına 5000 try transfer edilmeli' do
    @new_user.save

    assert_equal @new_user.try_balance, 5000
  end

  test 'btc metodu ile kullanıcının sahip olduğu btc gösterilmeli' do
    user = users(:joe)

    assert_equal user.btc_balance, user.currency_transactions.sum(:btc)
  end

  test 'try metodu ile kullanıcının sahip olduğu try gösterilmeli' do
    user = users(:joe)

    assert_equal user.try_balance, user.currency_transactions.sum(:try)
  end

  test 'oluşturulduğunda token alanı oluşturulmalı' do
    @new_user.save
    @new_user.reload

    assert_not_nil @new_user.token
  end

  test 'password alanı minimum 6 karaktere sahip olmalı' do
    # Parola 6 karakterden kısa olmamalı
    # Parola 20 karakterden uzun olmamalı

    (1..20).to_a.each do |i|
      @new_user.password = 'A' * i
      @new_user.password_confirmation = 'A' * i

      i >= 6 ? assert(@new_user.valid?) : refute(@new_user.valid?)
    end
  end

  test 'email boş olduğunda geçersiz olmalı' do
    @new_user.email = nil

    refute @new_user.valid?
  end

  test 'email alanı sadece email olabilmeli' do
    bad = %w(ba@mail no-good.com not_good_mail.email@email)
    good = %w(lorem@mail.com good@icloud.com)

    bad.each do |email|
      @new_user.email = email
      refute @new_user.valid?
    end

    good.each do |email|
      @new_user.email = email
      assert @new_user.valid?
    end
  end

  test 'email alanı benzersiz olmalı' do
    @new_user.email = users(:joe).email

    refute @new_user.valid?
  end

  test 'yeni kaydedilebilmeli' do
    assert @new_user.save
  end
end
