require 'test_helper'

class DocumentTest < ActiveSupport::TestCase
  setup do
    @new_document = Document.new(title: 'lorem', url: 'ipsum')
  end

  test 'yeter şartlar sağlandığında geçerli olmalı' do
    assert @new_document.valid?
  end

  test 'title alanı boş olmamalı' do
    @new_document.title.clear
    refute @new_document.valid?
  end

  test 'url alanı boş olmamalı' do
    @new_document.url.clear
    refute @new_document.valid?
  end
end
