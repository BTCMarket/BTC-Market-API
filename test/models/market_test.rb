require 'test_helper'

class MarketTest < ActiveSupport::TestCase
  # Market ile ilgili hesaplamalar yapılan modelin testleri.

  test 'lowest_sell istenen şekilde çalışmalı' do
    # En düşük satış emri orders(:sell) => 100.
    assert_equal Market.lowest_sell, orders(:sell).price
  end

  test 'highest_buy istenen şekilde çalışmalı' do
    # En yüksek alım emri  orders(:buy) => 95
    assert_equal Market.highest_buy, orders(:buy).price
  end

  test 'yesterdays_avg_price istenen şekilde çalışmalı' do
    assert_equal 0, Market.yesterdays_avg_price
  end

  test 'todays_avg_price istenen şekilde çalışmalı' do
    assert_equal (Order.sell.sum(:price) / Order.sell.count).to_f.round(2),
                 Market.todays_avg_price
  end
end
