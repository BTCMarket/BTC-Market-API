require 'test_helper'

class SendGeneralNotificationJobTest < ActiveJob::TestCase
  test 'komut verildiğinde tüm kullanıcılara duyuru gitmeli' do
    assert_difference('Notification.count', User.count) do
      SendGeneralNotificationJob.perform_now('Hello', 'World')

      # Job çalıştırıldığında sistemdeki her kullanıcıya bildirim gitmeli.

      User.all.each do |user|
        assert_equal user.notifications.unscoped.last.title, 'Hello'
        assert_equal user.notifications.unscoped.last.content, 'World'
        assert user.notifications.unscoped.last.general?
      end
    end
  end
end
