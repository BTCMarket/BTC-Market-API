ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'policy_assertions'

module ActiveSupport
  class TestCase
    fixtures :all

    setup { User.all.each(&:set_token!) }
  end
end
