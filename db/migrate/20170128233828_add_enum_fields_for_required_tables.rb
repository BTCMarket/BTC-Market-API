class AddEnumFieldsForRequiredTables < ActiveRecord::Migration[5.0]
  def up
    # Refactor Notifications
    remove_column :notifications, :viewed
    add_column :notifications, :status, :integer

    # Refactor Orders
    remove_column :orders, :is_active
    add_column :orders, :status, :integer
    remove_column :orders, :order_type
    add_column :orders, :order_status, :integer
  end

  def down
    # Refactor Notifications
    add_column :notifications, :viewed, :boolean
    remove_column :notifications, :status

    # Refactor Orders
    add_column :orders, :is_active, :boolean, default: true
    remove_column :orders, :status, :integer
    add_column :orders, :order_type, :string
    remove_column :orders, :order_status, :integer
  end
end
