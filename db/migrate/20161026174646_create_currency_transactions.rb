class CreateCurrencyTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :currency_transactions do |t|
      t.integer :user_id
      t.decimal :btc
      t.decimal :try

      t.timestamps
    end
  end
end
