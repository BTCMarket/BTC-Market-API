# Öncelikle tüm duyuruları silelim.
Notification.delete_all
puts 'Tüm duyurular silindi!'

# Tüm hesap hareketlerini silelim.
CurrencyTransaction.delete_all
puts 'Tüm transferler silindi!'

# Tüm emirleri silelim.
Order.delete_all
puts 'Tüm emirler silindi!'

# Tüm kullanıcıları silelim.
User.delete_all
puts 'Tüm kullanıcılar silindi!'

# Önce kullanıcıları oluşturalım.
10.times do
  user = User.new email: Faker::Internet.email,
                  password: '123456',
                  password_confirmation: '123456'
  user.save
  puts "Kullanıcı oluşturuldu: #{user.email}"

  # Sonra hesapların hepsini zengin edelim.
  user.currency_transactions.create btc: 40_000, try: 500_000
  puts 'Para transfer edildi: 40000 BTC, 500000 TRY'
end

# Şimdi de 1 aydan geriye doğru rastgele alım ve satım emirleri oluşturalım.

puts "Emirler oluşturuluyor..."
puts "TIP\tFIYAT\tADET"
(0..30).to_a.reverse.each do |day_num|
  user = User.all.sample

  2.times do
    sell = user.orders.new order_status: Order.order_statuses[:sell],
                           amount: 0.2 + rand(10)/10.0,
                           price: 1500 + rand(1000),
                           created_at: day_num.day.ago
    sell.save
    puts "SATIM\t#{sell.price}\t#{sell.amount}"

    buy = user.orders.new order_status: Order.order_statuses[:buy],
                          amount: 0.2 + rand(10)/10.0,
                          price: 750 + rand(650),
                          created_at: day_num.day.ago
    buy.save
    puts "[ALIM]\t#{buy.price}\t#{buy.amount}"
  end
end

# Yigit kullanıcısını oluşturalım.

User.create email: 'yigitsadic@gmail.com', password: '123456',
            password_confirmation: '123456'
