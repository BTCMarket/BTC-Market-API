Rails.application.routes.draw do
  namespace :v1 do
    get 'markets/status'

    get 'histories/transactions'
    get 'histories/orders'

    get 'graphs/monthly'

    resources :notifications, only: [:index, :show] do
      put 'set_viewed', on: :member
      get 'not_seen', on: :collection
    end

    resources :users, except: :index do
      get 'me', on: :collection
      get 'wallet', on: :collection
    end

    resources :orders, except: [:update, :destroy] do
      put 'cancel', on: :member
    end

    resources :documents, only: [:index, :show]
    resources :currency_transactions, only: [:index, :show]
    resources :tokens, only: :create
  end
end
